import { Routes, RouterModule } from '@angular/router';

import { HomeComponent }             from './home/home';

export const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    {
      path: 'home',
      component: HomeComponent
    },
    { path: '', loadChildren: 'app/accounting/accounting.module#AccountingModule'},
    { path: '', loadChildren: 'app/receiving/receiving.module#ReceivingModule'},
    { path: '', loadChildren: 'app/oe/oe.module#OeModule' },
    { path: '', loadChildren: 'app/shipping/oe.module#ShippingModule' },
];

export const appRoutingProviders: any[] = [

];

export const routing = RouterModule.forRoot(routes);
