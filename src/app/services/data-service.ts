import { Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

@Injectable()
export class DataService {

    constructor(public http: Http) { }

    get(uri: string, mapJson = true) {
        let http: any = this.http;

        if (mapJson) {
          return http.get(uri)
              .map((res: Response) => res.json());
        } else {
          return http.get(uri);
        }

    }

    post(uri: string, data?: any, mapJson = true) {
        let http: any = this.http;

        if (mapJson) {
          return http.post(uri, data)
              .map(response => <any>(<Response>response).json());
        } else {
          return http.post(uri, data);
        }
    }

    patch(uri: string, data?: any, mapJson = true) {
      let http: any = this.http;

      if (mapJson) {
        return http.patch(uri, data)
            .map(response => <any>(<Response>response).json());
      } else {
        return http.patch(uri, data);
      }

    }

    put(uri: string, data?: any, mapJson = true) {
      let http: any = this.http;

      if (mapJson) {
        return http.put(uri, data)
            .map(response => <any>(<Response>response).json());
      } else {
        return http.put(uri, data);
      }

    }

    delete(uri: string) {
        let http: any = this.http;

        return http.delete(uri)
            .map(response => <any>(<Response>response).json());
    }
}
