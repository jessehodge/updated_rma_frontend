import { Injectable } from '@angular/core';

import { DataService } from './data-service';

@Injectable()
export class APIService {
  public static localhost: string = '/api/v1';

  private _header_url: string = APIService.localhost + '/rma_header/';

  constructor (
    private dataService: DataService
  ) { }
  // Order Entry

  getRmaHeaderItems(id: number) {
    let url = this._header_url + id;
    return this.dataService.get(url);
  }

  getAccountingRmaInfo() {
    return this.dataService.get(this._header_url);
  }
}
