export class RmaLines {
  public id: number;
  public rma_header_id: number;
  public description: string;
  public description_2: string;
  public shipping_weight: number;
  public shipping_classification: string;
  public original_quantity: string;
  public expected_return_quantity: number;
  public returned_qty: string;
  public credit_quantity: string;
  public rts_qty: string;
  public rtm_qty: string;
  public scrap_qty: string;
  public return_completed: boolean;
  public returned_condition: string;
  public invoice_number: number;
  public amount_credited: string;

  constructor(obj?: any) {
    if (obj) {
      for (let prop in obj) {
        if (obj.hasOwnProperty(prop)) {
          this[prop] = obj[prop];
        }
      }
    }
  }
}
