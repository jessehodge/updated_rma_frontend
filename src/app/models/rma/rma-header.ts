export class RmaHeader {
  public id: number;
  public request_user_code: string; // Foreign Key to Salesperson_Purchaser
  public invoice_number: string; // Foreign Key to SalesInvoiceHeader
  public request_date: string; // Prepopulate as current date
  public call_tag: string;
  public credit_memo_required: boolean;
  // Foreign Key GET from SalesInvoiceHeader
  public ship_to_business_name: string;
  public ship_to_first_name: string;
  public ship_to_last_name: string;
  public ship_to_address: string;
  public ship_to_address_2: string;
  public ship_to_city: string;
  public ship_to_county: string;
  public ship_to_post_code: string;
  public ship_to_email: string;
  public ship_to_phone_number: string;
  public payment_terms_code: string;
  // Manual Entry
  public pick_up_day: Date;
  public pick_up_hours: string;
  public return_reason: string;
  public return_method: string;
  public return_address: string;
  public refund_instructions: string;
  public cm_restocking_charge: number;
  public cm_deduct_outgoing_freight: boolean;
  public cm_deduct_incoming_freight: boolean;
  public pick_up_residential: boolean;
  public pick_up_notes: string;
  public fedex_call_tag: string;
  public received_complete: boolean;
  public received_notes: string;
  public credit_notes: string;
  public credit_memo_number: string;
  public status: string;
  public call_tag_number: string;
  public all_items_accounted_for: boolean;

  constructor(obj?: any) {
    if (obj) {
      for (let prop in obj) {
        if (obj.hasOwnProperty(prop)) {
          this[prop] = obj[prop];
        }
      }
    }
  }
}
