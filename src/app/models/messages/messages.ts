export class RmaMessages {
  public id: number;
  public rma_header_id: number;
  public date_posted: string;
  public department: string;
  public sender: string;
  public text: string;

  constructor(obj?: any) {
    if (obj) {
      for (let prop in obj) {
        if (obj.hasOwnProperty(prop)) {
          this[prop] = obj[prop];
        }
      }
    }
  }
}
