export class PalletHeader {
  public id: number;
  public pallet_length: string;
  public pallet_width: string;
  public pallet_height: string;
  public pallet_shipping_class: string;
  public pallet_weight: string;
  public pallet_barcode: string;
  public pallet_number_of_boxes: string;

  constructor(obj?: any) {
    if (obj) {
      for (let prop in obj) {
        if (obj.hasOwnProperty(prop)) {
          this[prop] = obj[prop];
        }
      }
    }
  }
}
