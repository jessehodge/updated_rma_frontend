import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { OERoutes }         from './oe.routes';
import { OeService }        from './oe.service';

import { OeHomeComponent }           from './oe.home';
import { OeComponent }      from './oe.component';
import { OeRmaHomeComponent }        from './rma/oe.rma.home';
import { OeRmaComponent }            from './rma/oe.rma';
import { OeEditRmaTableComponent }   from './rma/tables/edit/edit-rma.table';
import { OeRmaDetailComponent }      from './rma/detail/oe.rma-detail';
import { OETableComponent }          from './rma/tables/oe.rma.table';
import { OeCurrentRmaTableComponent }from './rma/tables/current/oe.current-rma.table';


import { MessagesModule }   from '../messages/messages.module';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MessagesModule,
    OERoutes
  ],
  declarations: [
    OeComponent,
    OeHomeComponent,
    OeRmaHomeComponent,
    OeRmaComponent,
    OETableComponent,
    OeCurrentRmaTableComponent,
    OeRmaDetailComponent,
    OeEditRmaTableComponent
  ],
  providers: [OeService]
})
export class OeModule { }
