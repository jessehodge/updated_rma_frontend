import { RouterModule } from '@angular/router';
import { OeComponent }  from './oe.component';
import { OeHomeComponent }       from './oe.home';
import { OeRmaHomeComponent }    from './rma/oe.rma.home';
import { OeRmaComponent }        from './rma/oe.rma';
import { OeRmaDetailComponent }  from './rma/detail/oe.rma-detail';

export const OERoutes = RouterModule.forChild([
  { path: 'oe', component: OeComponent, children: [
      { path: '', component: OeHomeComponent },
      { path: 'rma', component: OeRmaHomeComponent },
      { path: 'rma/:psi', component: OeRmaComponent },
      { path: 'rma/detail/:id', component: OeRmaDetailComponent }
    ]
  }
]);
