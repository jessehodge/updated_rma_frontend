import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { OeService } from '../oe.service';

import { RmaHeader } from '../../models/rma/rma-header';
import { SalesInvoiceHeader } from '../../models/sales/sales-invoice-header';

@Component({
  selector: 'app-oe-rma-home',
  templateUrl: './oe.rma.home.html',
})

export class OeRmaHomeComponent implements OnInit {
  public id: number;
  public rma_headers = new Array<RmaHeader>();

  public sales_invoice_header: SalesInvoiceHeader = new SalesInvoiceHeader();

  constructor(
    private service: OeService,
    private _router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.activatedRoute.params.subscribe(p => {
      let id = p['id'];
      if (id) {
        this.id = id;
      }
    });
  }

  ngOnInit() {
    this.service.OeGetRmaList().subscribe(
      res => {
        this.rma_headers = res;
      },
      err => {
        console.error(err);
      }
    );
  }

  public onOePsiSubmit() {
    this._router.navigate(['/oe/rma/', this.sales_invoice_header.no_field], { relativeTo: this.activatedRoute });
  }

  public OnRmaHeaderClick(rma_header) {
    this._router.navigate(['/oe/rma/detail/', rma_header.id], {relativeTo: this.activatedRoute});
  }

}
