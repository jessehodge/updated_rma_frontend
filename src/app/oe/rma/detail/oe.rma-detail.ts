import { Component, OnInit }  from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { OeService }          from '../../oe.service';

import { RmaHeader }          from '../../../models/rma/rma-header';

@Component({
  selector: 'app-oe-rma-detail',
  templateUrl: './oe.rma-detail.html'
})

export class OeRmaDetailComponent implements OnInit {
  public rma_header = new RmaHeader();
  public rma_header_id: number;

  constructor(
    private service: OeService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.activatedRoute.params.subscribe(p => {
      let id = p['id'];
      if (id) {
        this.rma_header_id = id;
      }
    });
  }

  ngOnInit() {
    this.service.OeGetRmaHeader(this.rma_header_id).subscribe(
      res => {
        this.rma_header = res;
        console.log(res);
      },
      err => {
        console.error(err);
      }
    );
  }

  public oeRmaHeaderSubmit() {
    this.service.OeRmaHeaderPut(this.rma_header).subscribe(
      res => {
        console.log(res);
      },
      err => {
        console.error(err);
      }
    );
  }
}
