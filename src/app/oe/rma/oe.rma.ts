import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { RmaHeader } from '../../models/rma/rma-header';
import { OeService } from '../oe.service';

@Component({
  selector: 'app-add-rma',
  templateUrl: './oe.rma.html'
})

export class OeRmaComponent implements OnInit {
  public rma_header = new Array<RmaHeader>();
  public psi: number;
  public rma_header_id: number;

  constructor(
    private _router: Router,
    private activatedRoute: ActivatedRoute,
    private service: OeService
  ) {
    this.activatedRoute.params.subscribe(p => {
      let psi = p['psi'];
      if (psi) {
        this.psi = psi;
      }
    });

  }

  public ngOnInit() {
    this.service.OeGetInvoiceInfo(this.psi).subscribe(
      res => {
        this.rma_header = res;
      },
      err => {
        alert('That invoice doesn\'t exist please re-enter the invoice number you are looking for.');
        this._router.navigate(['/oe/rma']);
      }
    );
  }

  public onOrderEntryRmaHeaderSubmit() {
    this.service.OeRmaHeaderPost(this.rma_header).subscribe(
      res => {
        this.rma_header_id = res.id;
      },
      err => {
        console.error(err);
      }
    );
  };

  public oeCompletedRma() {
    alert('Your return has been completed!');
    this._router.navigate(['/oe/rma']);
  }

}
