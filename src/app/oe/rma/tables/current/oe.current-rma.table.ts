import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { OeService } from '../../../oe.service';

import { RmaLines } from '../../../../models/rma/rma-lines';

@Component({
  selector: 'app-oe-current-rma-table',
  templateUrl: './oe.current-rma.table.html'
})

export class OeCurrentRmaTableComponent implements OnInit {
  @Input() psi: number;
  @Input() rma_header_id: number;
  @Input() rma_lines: Array<RmaLines>;

  constructor(
    private service: OeService,
    private activatedRoute: ActivatedRoute,
    private _router: Router
  ) {
    this.activatedRoute.params.subscribe(p => {
      let id = p['id'];
      if (id) {
        this.psi = id;
        this.rma_header_id = id;
      }
    });
  }

  public getRmaInvoiceItems() {
    this.service.OeGetRmaInvoiceItems(this.psi).subscribe(
      res => {
        this.rma_lines = res.map(line => {
          return {
            id: line.id,
            rma_header_id: line.rma_header_id,
            description: line.description,
            description_2: line.description_2,
            shipping_weight: parseFloat(line.shipping_weight),
            shipping_classification: line.shipping_classification,
            original_quantity: +line.original_quantity,
            expected_return_quantity: +line.expected_return_quantity,
            returned_qty: +line.returned_qty,
            credit_quantity: +line.credit_quantity,
            rts_qty: +line.rts_qty,
            rtm_qty: +line.rtm_qty,
            scrap_qty: +line.scrap_qty,
            return_completed: line.return_completed,
            returned_condition: line.returned_condition,
            invoice_number: +line.invoice_number,
            amount_credited: line.amount_credited
          };
        });
      },
      err => {
        console.log(err);
      }
    );
  }

  ngOnInit() {
    this.getRmaInvoiceItems();
  }

  public onOeLineItemReturnedSubmit() {
    let credited_items = this.rma_lines.filter(r => r.amount_credited != null);
    let credited_ids = [];
    console.log(credited_items);
    let received_items_id = credited_ids.push(credited_items.forEach(x => x.id));
    this.service.OePutAllItemsFinal(credited_items).subscribe(
      res => {
        console.log(res);
      },
      err => {
        console.error(err);
      }
    );
  }

  public getRmaDescription(obj: any): string {
    return obj.description + ' ' + obj.description_2;
  }

  public setRmaDescription(obj: any, val: any) {
    obj.description = val;
    obj.description_2 = '';
  }
}
