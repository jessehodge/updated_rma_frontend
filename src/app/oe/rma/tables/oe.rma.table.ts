import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { RmaLines } from '../../../models/rma/rma-lines';

import { OeService } from '../../oe.service';

@Component({
  selector: 'app-oe-table',
  templateUrl: './oe.rma.table.html'
})

export class OETableComponent implements OnInit {
  @Input() psi: number;
  @Input() rma_header_id: number;
  public rma_lines = new Array<RmaLines>();
  public submitted = false;

  constructor(
    private service: OeService,
    private _router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.service.OeGetItems(this.psi).subscribe(
      res => {
        this.rma_lines = res.map(line => {
          return {
            id: line.id,
            rma_header_id: this.rma_header_id,
            description: line.description,
            description_2: line.description_2,
            shipping_weight: parseFloat(line.shipping_weight),
            shipping_classification: line.shipping_classification,
            original_quantity: +line.original_quantity,
            expected_return_quantity: line.expected_return_quantity ? +line.expected_return_quantity : 0,
            returned_qty: +line.returned_qty,
            credit_quantity: +line.credit_quantity,
            rts_qty: +line.rts_qty,
            rtm_qty: +line.rtm_qty,
            scrap_qty: +line.scrap_qty,
            return_completed: line.return_completed,
            returned_condition: line.returned_condition,
            invoice_number: +line.invoice_number,
            amount_credited: line.amount_credited
          };
        });
      },
      err => {
        console.error(err);
      });
  }

  public onOrderEntryRmaLineSubmit() {
    let post_lines = this.rma_lines.filter(r => r.expected_return_quantity > 0);
    post_lines.forEach(
      pl => pl.rma_header_id = this.rma_header_id,
      pl => pl.invoice_number = this.psi
    );
    this.service.OeRmaLinePost(post_lines).subscribe(
      res => {
        this.submitted = true;
        console.log(post_lines);
      },
      err => {
        console.error(err);
      }
    );
  }


}
