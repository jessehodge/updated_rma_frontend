import { Component } from '@angular/core';

@Component({
  selector: 'app-oe',
  template: `<router-outlet></router-outlet>`
})
export class OeComponent {

}
