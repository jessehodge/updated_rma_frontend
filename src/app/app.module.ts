import { NgModule }                 from '@angular/core';
import { BrowserModule  }           from '@angular/platform-browser';
import { FormsModule }              from '@angular/forms';

import { OeModule }                 from './oe/oe.module';
import { ReceivingModule }          from './receiving/receiving.module';
import { AccountingModule }         from './accounting/accounting.module';
import { MessagesModule }           from './messages/messages.module';
import { ShippingModule }           from './shipping/shipping.module';

import { AppComponent }             from './app.component';
import { HomeComponent }                     from './home/home';

import { APIService }               from './services/api-service';
import { DataService }              from './services/data-service';

import {
  BaseRequestOptions,
  Headers, RequestOptions }         from '@angular/http';

import { routing }                  from './app.routes';
import { HttpModule, JsonpModule }  from '@angular/http';

export class AppBaseRequestOptions extends BaseRequestOptions {
  private authKey = localStorage.getItem('token');
  headers: Headers = new Headers({
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': 'Token ' + this.authKey,
    'Accept': 'application/json'
  });
};

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  providers: [
    APIService,
    DataService,
    {provide:
      RequestOptions, useClass: AppBaseRequestOptions}
  ],
  imports: [
    BrowserModule,
    FormsModule,
    routing,
    HttpModule,
    JsonpModule,
    OeModule,
    ReceivingModule,
    AccountingModule,
    ShippingModule,
    MessagesModule.forRoot()
  ],

    bootstrap: [ AppComponent ],
})

export class AppModule {}
