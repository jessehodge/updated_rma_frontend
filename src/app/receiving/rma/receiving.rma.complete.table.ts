import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { ReceivingService } from '../receiving.service';
import { RmaHeader } from '../../models/rma/rma-header';

@Component({
  selector: 'app-complete-rma-table',
  templateUrl: './receiving.rma.complete.table.html'
})

export class ReceivingCompleteRmaHeaderComponent implements OnInit {
  private complete_header_id: number;
  private complete_headers = new Array<RmaHeader>();

  constructor(
    private _router: Router,
    private service: ReceivingService,
    private activatedRoute: ActivatedRoute
  ) {
    this.activatedRoute.params.subscribe(p => {
      let id = p['id'];
      if (id) {
        this.complete_header_id = id;
      }
    });
  }

  ngOnInit() {
    this.service.getReceivingCompleteRmaList().subscribe(
      res => {
        this.complete_headers = res;
      },
      err => {
        alert('Couldn\'t retrieve the Complete Rma List!');
      }
    );
  }

  public OnCompleteRmaHeaderClick(complete_header) {
    this._router.navigate(['./', complete_header.id], {relativeTo: this.activatedRoute});
  }

}
