import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ReceivingService } from '../receiving.service';

import { RmaHeader } from '../../models/rma/rma-header';
import { RmaLines } from '../../models/rma/rma-lines';

@Component({
  selector: 'app-receiving-rma-detail',
  templateUrl: './receiving.rma.detail.html'
})

export class ReceivingRmaDetailComponent implements OnInit {
  public id: number;
  public rma_header_id: number;
  public rma_header = new RmaHeader();
  public rma_lines = new Array<RmaLines>();

  constructor(
    private service: ReceivingService,
    private activatedRoute: ActivatedRoute,
    private _router: Router
  ) {
    this.activatedRoute.params.subscribe(p => {
      let id = p['id'];
      if (id) {
        this.id = id;
        this.rma_header_id = id;
      }
    });
  }

  ngOnInit() {
    this.service.getReceivingHeaderDetail(this.id).subscribe(
      res => {
        this.rma_header = res;
      },
      err => {
        alert('I could not retrieve the Header Detail Information!');
      }
    );
    this.service.getReceivingRmaHeaderItems(this.id).subscribe(
      res => {
        this.rma_lines = res;
      },
      err => {
        alert('I could not retrieve the Rma Header Items information!');
      }
    );
  }

  onReceivingRmaLineSubmit() {
    let received_items = this.rma_lines.filter(r => r.expected_return_quantity > 0);
    let receieved_ids = [];
    let received_items_id = receieved_ids.push(received_items.forEach(x => x.id));
    this.service.ReceivedItemPut(received_items, received_items_id).subscribe(
      res => {
        console.log(res);
      },
      err => {
        alert('Something went wrong... Please contact the administrator and screenshot this error: ' + err);
      }
    );
  }

  onHeaderStatusSubmit() {
    status = JSON.stringify({status: this.rma_header.status});
    let received_header_id = this.rma_header.id;
    this.service.ReceivedHeaderStatus(status, received_header_id).subscribe(
      res => {
        alert('The order is currently: ' + this.rma_header.status);
      },
      err => {
        alert('Something went wrong... Please contact the administrator and screenshot this error: ' + err);
      }
    );
  }

}
