import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { ReceivingService } from '../receiving.service';
import { RmaHeader } from '../../models/rma/rma-header';

@Component({
  selector: 'app-receiving-table',
  templateUrl: './receiving.rma.table.html'
})

export class ReceivingRmaTableComponent implements OnInit {
  private id: number;
  private rma_headers = new Array<RmaHeader>();

  constructor(
    private _router: Router,
    private service: ReceivingService,
    private activatedRoute: ActivatedRoute
  ) {
    this.activatedRoute.params.subscribe(p => {
      let id = p['id'];
      if (id) {
        this.id = id;
      }
    });
  }

  ngOnInit() {
    this.service.getReceivingRmaList().subscribe(
      res => {
        this.rma_headers = res;
      },
      err => {
        alert('Couldnt retrieve the Receiving Rma List!');
      }
    );
  }

  public OnRmaHeaderClick(rma_header) {
    this._router.navigate(['./', rma_header.id], {relativeTo: this.activatedRoute});
  }

}
