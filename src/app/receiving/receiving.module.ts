import { NgModule } from '@angular/core';
import { CommonModule }             from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ReceivingRoutes }          from './receiving.routes';
import { ReceivingHomeComponent }            from './receiving.home';
import { ReceivingComponent }       from './receiving.component';
import { ReceivingRmaComponent }             from './rma/receiving.rma';
import { ReceivingCompleteRmaHeaderComponent } from './rma/receiving.rma.complete.table';
import { ReceivingRmaTableComponent }        from './rma/receiving.rma.table';
import { ReceivingRmaDetailComponent }       from './rma/receiving.rma.detail';
import { ReceivingService }         from './receiving.service';

import { MessagesModule }   from '../messages/messages.module';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    ReceivingRoutes,
    MessagesModule
  ],
  declarations: [
    ReceivingComponent,
    ReceivingHomeComponent,
    ReceivingRmaComponent,
    ReceivingCompleteRmaHeaderComponent,
    ReceivingRmaTableComponent,
    ReceivingRmaDetailComponent
  ],
  providers: [ReceivingService]
})
export class ReceivingModule { }
