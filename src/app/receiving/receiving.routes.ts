import { RouterModule } from '@angular/router';
import { ReceivingComponent } from './receiving.component';
import { ReceivingHomeComponent } from './receiving.home';
import { ReceivingRmaComponent } from './rma/receiving.rma';
import { ReceivingRmaDetailComponent } from './rma/receiving.rma.detail';

export const ReceivingRoutes = RouterModule.forChild([
  { path: 'receiving', component: ReceivingComponent, children: [
      { path: '', component: ReceivingHomeComponent },
      { path: 'rma', component: ReceivingRmaComponent },
      { path: 'rma/:id', component: ReceivingRmaDetailComponent }
    ]
  }
]);
