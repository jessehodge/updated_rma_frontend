import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AccountingRoutes }         from './accounting.routes';
import { AccountingService }        from './accounting.service';

import { AccountingHomeComponent }  from './accounting.home';
import { AccountingComponent }      from './accounting.component';
import { AccountingRmaComponent }            from './rma/accounting.rma';
import { AccountingRmaTableComponent }       from './rma/accounting.rma.table';
import { AccountingRmaDetailComponent } from './rma/accounting.rma.detail';

import { MessagesModule }   from '../messages/messages.module';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    AccountingRoutes,
    MessagesModule
  ],
  declarations: [
    AccountingComponent,
    AccountingHomeComponent,
    AccountingRmaComponent,
    AccountingRmaTableComponent,
    AccountingRmaDetailComponent
  ],
  providers: [AccountingService]
})
export class AccountingModule { }
