import { Injectable } from '@angular/core';
import { DataService } from '../services/data-service';
import { APIService } from '../services/api-service';

@Injectable()
export class AccountingService {
  private _rma_header:                string = APIService.localhost + '/rma_header/';
  private _accounting_rma_header_url: string = APIService.localhost + '/rma_header/accounting/';
  private _rma_line_url:              string = APIService.localhost + '/rma_line/';
  private _receiving_lines_url:       string = APIService.localhost + '/rma_line/receiving/';


  public getAccountingRmaList() {
    return this.dataService.get(this._accounting_rma_header_url);
  }

  public getAccountingHeaderDetail(id: number) {
    let url = this._rma_header + id + '/';
    return this.dataService.get(url);
  }

  public getAccountingRmaHeaderItems(id: number) {
    let url = this._receiving_lines_url + id + '/';
    return this.dataService.get(url);
  }

  public putAccountingItems(rma_lines) {
    let url = this._rma_line_url;
    return this.dataService.put(url, rma_lines);
  }

  public putAccountingCreditMemo(rma_header, accounting_header_id) {
    let url = this._rma_header + accounting_header_id + '/';
    return this.dataService.put(url, rma_header);
  }

  constructor(private dataService: DataService) {}
}
