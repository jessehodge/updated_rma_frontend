import { NgModule }                         from '@angular/core';
import { CommonModule }                     from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ShippingRoutes }                   from './shipping.routes';
import { ShippingService }                  from './shipping.service';

import { ShippingHomeComponent }            from './shipping.home';
import { ShippingComponent }                from './shipping.component';
import { PalletsHomeComponent }             from './pallets/pallets.home.component';
import { PalletCreationHomeComponent }      from './pallets/pallet-creation/pallet-creation.home.component';
import { LoadingHomeComponent }             from './loading/loading.home.component';
import { TestLoadHomeComponent }            from './test-load/test-load.home.component';


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    ShippingRoutes
  ],
  declarations: [
    ShippingComponent,
    ShippingHomeComponent,
    PalletsHomeComponent,
    PalletCreationHomeComponent,
    LoadingHomeComponent,
    TestLoadHomeComponent
  ],
  providers: [ShippingService]
})
export class ShippingModule { }
