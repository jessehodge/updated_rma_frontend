import { Component } from '@angular/core';

@Component({
  selector: 'app-shipping',
  template: `<router-outlet></router-outlet>`
})
export class ShippingComponent {

}
