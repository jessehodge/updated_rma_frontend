import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { PalletHeader } from '../../models/pallets/pallet-header';

import { ShippingService } from '../shipping.service';

@Component({
  selector: 'app-pallets-home',
  templateUrl: './pallets.home.component.html'
})

export class PalletsHomeComponent implements OnInit {
  public id: number;
  public pallets = new Array<PalletHeader>();


  constructor(
    private service: ShippingService,
    private _router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.activatedRoute.params.subscribe(p => {
      let id = p['id'];
      if (id) {
        this.id = id;
      }
    });
  }

  ngOnInit() {
    this.service.ShippingGetPalletList().subscribe(
      res => {
        this.pallets = res;
      },
      err => {
        console.error(err);
      }
    );
  }

  public OnPalletClick(pallet) {
    this._router.navigate(['/shipping/pallets/detail/', pallet.id], {relativeTo: this.activatedRoute});
  }

}
