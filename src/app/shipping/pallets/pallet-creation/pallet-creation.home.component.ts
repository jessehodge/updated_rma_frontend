import { Component } from '@angular/core';

import { ShippingService } from '../../shipping.service';

import { PalletHeader } from '../../../models/pallets/pallet-header';

@Component({
  selector: 'app-pallet-creation-home',
  templateUrl: './pallet-creation.home.component.html'
})

export class PalletCreationHomeComponent {
  public pallets = new Array<PalletHeader>();
  public pallet = new PalletHeader();

  constructor(
    private service: ShippingService,
  ) { }

  onPalletCreate(pallet) {
    console.log(pallet);
    this.service.ShippingCreatePallet(pallet).subscribe(
      res => {
        console.log(res);
        alert('You created a pallet!');
      },
      err => {
        console.error(err);
      }
    );
  }
}
