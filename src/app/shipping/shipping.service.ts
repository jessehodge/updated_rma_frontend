import { Injectable } from '@angular/core';
import { DataService } from '../services/data-service';
import { APIService } from '../services/api-service';

@Injectable()
export class ShippingService {

  private _pallets_url: string = APIService.localhost + '/pallet_header/';

// POST Requests
  public ShippingCreatePallet(pallet) {
    let url = this._pallets_url;
    return this.dataService.post(url, pallet);
  }
// GET Requests
  public ShippingGetPalletList() {
    let url = this._pallets_url;
    return this.dataService.get(url);
  }

  constructor(private dataService: DataService) {}
}
