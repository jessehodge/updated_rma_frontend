import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MessagesService } from './messages.service';
import { RmaMessages } from '../models/messages/messages';

@Component({
  selector: 'app-messages',
  templateUrl: 'messages.component.html'
})
export class MessagesComponent implements OnInit {
  public messages = new Array<RmaMessages>();
  public message = new RmaMessages();
  @Input() rma_header_id: number;
  @Input() id: number;

  constructor(
    private service: MessagesService,
    private activatedRoute: ActivatedRoute
  ) {
    this.activatedRoute.params.subscribe(p => {
      let id = p['id'];
      if (id) {
        this.rma_header_id = id;
        this.id = id;
      }
    });
  }

  ngOnInit() {
      this.service.getMessages(this.id).subscribe(
        res => {
          this.messages = res;
        },
        err => {
          console.log('There are no messages.');
        }
      );
    }

  postMessage(message) {
    message.rma_header_id = this.rma_header_id;
    this.service.addMessage(message).subscribe(
      res => {
        this.messages = this.messages.concat([message]);
      },
      err => {
        console.error(err);
      }
    );
  }
}
