import { Injectable } from '@angular/core';
import { DataService } from '../services/data-service';
import { APIService } from '../services/api-service';

@Injectable()
export class MessagesService {
  private MessagesAPI: string = APIService.localhost + '/messages/';
  private DetailMessagesAPI: string = APIService.localhost + '/messages/detail/';

    constructor(private dataService: DataService) {}

    public getMessages(id: number) {
      let url = this.DetailMessagesAPI + id + '/';
      return this.dataService.get(url);
    }

    public addMessage(message) {
      return this.dataService.post(this.MessagesAPI, message);
    }
}
